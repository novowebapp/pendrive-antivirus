dim scriptdir

scriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)

startUsbService()
sub startUsbService()
    strComputer = "."
    Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
    Set wmiEvent = objWMIService.ExecNotificationQuery( _
        "Select * From __InstanceCreationEvent Within 1" & _
            " Where TargetInstance ISA 'Win32_PnPEntity' and" & _
                " TargetInstance.Description='USB Mass Storage Device'")

    While True

        Set objEvent = wmiEvent.NextEvent()
        Set objUSB = objEvent.TargetInstance
        strName = objUSB.Name

        strDeviceID = objUSB.DeviceID
        Set objUSB = Nothing
    
        Set colDrives = objWMIService.ExecQuery( _
            "Select * From Win32_LogicalDisk Where DriveType = 2")
    
        For Each objDrive in colDrives
            strDriveLetter = objDrive.DeviceID
        Next
        Set colDrives = Nothing

        WScript.Echo strName & " was mounted as " & strDriveLetter

        iniciandoConversaoUSB(strDriveLetter)

    Wend
    Set wmiEvent = Nothing
    Set objWMIService = Nothing

end sub

sub iniciandoConversaoUSB(strDriveLetter)

    WScript.Echo strDriveLetter
    Dim objShell
    Set objShell = Wscript.CreateObject("WScript.Shell")
    objShell.Run scriptdir+"\Fat32ToNTFS.vbs "+strDriveLetter+" "
    Set objShell = Nothing

end sub