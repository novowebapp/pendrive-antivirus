dim pb


Sub setProgress(percentComplete)
    pb.SetTitle("Step 2 of 5")
    pb.SetText("Copying bin/Release Folder")
    pb.Update(percentComplete)
end sub


Class ProgressBar
    Dim percentComplete

    Private m_PercentComplete
    Private m_CurrentStep
    Private m_ProgressBar
    Private m_Title
    Private m_Text
    Private m_StatusBarText
     
    'Initialize defaults
    Private Sub ProgessBar_Initialize
    m_PercentComplete = 0
    m_CurrentStep = 0
    m_Title = "Progress"
    m_Text = ""
    End Sub
     
    Public Function SetTitle(pTitle)
    m_Title = pTitle
    End Function
     
    Public Function SetText(pText)
    m_Text = pText
    End Function
     
    Public Function Update(percentComplete)
    m_PercentComplete = percentComplete
    UpdateProgressBar()
    End Function
     
    Public Function Show()
    Set m_ProgressBar = CreateObject("InternetExplorer.Application")
    'in code, the colon acts as a line feed
    m_ProgressBar.navigate2 "about:blank" : m_ProgressBar.width = 315 : m_ProgressBar.height = 40 : m_ProgressBar.toolbar = false : m_ProgressBar.menubar = false : m_ProgressBar.statusbar = false : m_ProgressBar.visible = 1
    m_ProgressBar.document.write "<body Scroll=no style='margin:0px;padding:0px;'><div style='text-align:center;'><span name='pc' id='pc'>0</span></div>"
    m_ProgressBar.document.write "<div id='statusbar' name='statusbar' style='border:1px solid blue;line-height:10px;height:10px;color:blue;'></div>"
    m_ProgressBar.document.write "<div style='text-align:center'><span id='text' name='text'></span></div>"
    End Function
     
    Public Function Close()
    m_ProgressBar.quit
    m_ProgressBar = Nothing
    End Function
     
    Private Function UpdateProgressBar()
    If m_PercentComplete = 0 Then
    m_StatusBarText = ""
    End If
    For n = m_CurrentStep to m_PercentComplete - 1
    m_StatusBarText = m_StatusBarText & "|"
    m_ProgressBar.Document.GetElementById("statusbar").InnerHtml = m_StatusBarText
    m_ProgressBar.Document.title = n & "% Complete : " & m_Title
    m_ProgressBar.Document.GetElementById("pc").InnerHtml = n & "% Complete : " & m_Title
    wscript.sleep 10
    Next
    m_ProgressBar.Document.GetElementById("statusbar").InnerHtml = m_StatusBarText
    m_ProgressBar.Document.title = m_PercentComplete & "% Complete : " & m_Title
    m_ProgressBar.Document.GetElementById("pc").InnerHtml = m_PercentComplete & "% Complete : " & m_Title
    m_ProgressBar.Document.GetElementById("text").InnerHtml = m_Text
    m_CurrentStep = m_PercentComplete
    End Function
     
    End Class