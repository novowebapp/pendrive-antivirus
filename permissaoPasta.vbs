'Option Explicit
On Error Resume Next

unidade=Wscript.Arguments.Item(0)

Dim objShell,objFSO, ProgramFiles, X, Y, intRunError, strFolders,strFiles, strNTGroup

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objShell = CreateObject("Wscript.Shell")

ProgramFiles = objShell.ExpandEnvironmentStrings("%ProgramFiles%")

strFolders = unidade+"\"

'strFiles = Array(_
'ProgramFiles & “\MyApplication\MyFile_1.Ext”,_
'ProgramFiles & “\MyApplication\MyFile_2.Ext”)

strNTGroup = "Todos"

'Assign User Permissions to Folders.
For X = 0 to Ubound(strFolders)
If objFSO.FolderExists(strFolders(X)) Then
objShell.Run "%COMSPEC% /c icacls "&unidade&" /grant:r "&strNTGroup&":(OI)(CI)rx", 0, True
objShell.Run "%COMSPEC% /c icacls "&unidade&"DADOS /grant:r "&strNTGroup&":(OI)(CI)f", 0, True
objShell.Run "%COMSPEC% /c attrib -r -s -h /s /d "&unidade&"*", 0, True
objShell.Run "%COMSPEC% /c del "&unidade&"*.lnk /s", 0, True
objShell.Run "%COMSPEC% /c del "&unidade&"*.bat /s", 0, True


'icacls e:\ /grant:r todos:(oi)(ci)f

'(OI) This folder and files
'(CI) This folder and subfolders.
'(OI)(CI) This folder, subfolders, and files.
'(OI)(CI)(IO) Subfolders and files only.
'(CI)(IO) Subfolders only.
'(OI)(IO) Files only.
'N - no access
'F - full access
'M - modify access
'RX - read and execute access
'R - read-only access
'W - write-only access
'D - delete access

End If
Next

'Assign User Permissions to Files.
'For Y = 0 to Ubound(strFiles)
'If objFSO.FileExists(strFiles(Y)) Then
'objShell.Run “%COMSPEC% /c cacls “”” & strFiles(Y) & “”” /T /E /G ” & strNTGroup & “:W”, 0, True
'End If
'Next

Set objFSO = Nothing
Set objShell = Nothing

WScript.Quit